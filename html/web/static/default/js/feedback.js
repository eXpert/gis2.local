$(document).ready(function () {
    $('.btn-submit').attr('disabled', true);
    $('.field-feedback-msg_type').hide();
    $('#feedback-fio').on('keyup', checkFIOField);
    $('#feedback-fio').on('blur', checkFIOField);

    $('#feedback-phone').on('keyup', checkPHONEField);
    $('#feedback-phone').on('blur', checkPHONEField);

    $('#feedback-email').on('keyup', checkEMAILField);
    $('#feedback-email').on('blur', checkEMAILField);

    $('#Feedback').on('submit', function () {
        if ($('#Feedback').find('div.has-error').length > 0) {
            return false;
        }
    });

    $('#Feedback').find("input[aria-required='true'],textarea[aria-required='true']").each(function () {
        if (
            $(this).attr('id') != 'feedback-fio'
            && $(this).attr('id') != 'feedback-phone'
            && $(this).attr('id') != 'feedback-email'
        ) {
            $(this).on('blur', function () {
                checkRequiredField($(this));
            });
            $(this).on('keyup', function () {
                checkRequiredField($(this));
            });
        }
    });

    /**
     * autocompleter class for ymap street geocoding
     */
    $(".autocompleter").on('blur', function () {
        //checkRequiredField($(this));
    });
    var $request2;
    $(".autocompleter").on('keyup', function (e) {

        var $el = $(this);
        var value = $el.val();
        if (value.length < 5) return false;
        e.stopPropagation();

        if ((value.length < 5) && (e.which == 8 || e.which == 46)) {// delete/backspace controls pressed
            $el.parent().removeClass('open');
            $el.next().empty();
            return true;
        }

        if (e.which == 40 || e.which == 38) {// left/right controls pressed
            return true;
        }


        if (e.which == 13 || e.which == 27) {// Enter/Esc controls pressed
            $el.blur();
            return false;
        }

        if (value.length <= 0 || $el.data('min') && value.length < $el.data('min')) {
            $el.parent().removeClass('open');
            return true;
        }

        if ($request2 != null) {
            $request2.abort();
            $request2 = null;
        }
        $request2 = $.ajax({
            url: 'https://geocode-maps.yandex.ru/1.x/?geocode=' +
            window.utils.Cookie.get('country_ru') +
            ', город ' + $el.val(),
            data: {'format': 'json', 'results': '10'},
            dataType: "json",
            type: "GET",
            success: function (data) {

                if (data.response.GeoObjectCollection.metaDataProperty.GeocoderResponseMetaData.found > 0) {
                    var ymapsdata = data.response.GeoObjectCollection.featureMember;
                    //for (var i in ymapsdata) { alert(ymapsdata[i].GeoObject.name);  }
                }

                if (data.response.GeoObjectCollection.metaDataProperty.GeocoderResponseMetaData.found > 0) {
                    if ($el.next().is('.dropdown-menu')) {
                        $el.next().empty();
                    } else {
                        $('<ul class="dropdown-menu"></ul>').insertAfter($el);
                        var activeIndex = -1;
                        $(document).keydown(function (event) {
                            if ($('.open .dropdown-menu').length > 0) {
                                var charCode = event.which;
                                var UL = $('.open .dropdown-menu:last');

                                switch (charCode) {
                                    case 13: // Enter
                                        if (UL.find('li.active').length > 0) {
                                            var option = UL.find('li.active a');
                                            $el.val(option.data('value'));
                                            $('#feedback-city').val(option.data('value'));
                                        }
                                        $el.parent().removeClass('open');
                                        break;
                                    case 27: // Esc
                                        $el.parent().removeClass('open');
                                        break;
                                    case 38: //Up
                                        event.preventDefault();
                                        if (activeIndex <= 0) activeIndex = 1;
                                        activeIndex--;
                                        UL.find('li').removeClass('active');
                                        UL.find('li:eq(' + activeIndex + ')').addClass('active');
                                        break;
                                    case 40: //Down
                                        event.preventDefault();
                                        activeIndex++;
                                        if (activeIndex > UL.find('li').length) activeIndex = UL.find('li').length - 1;
                                        UL.find('li').removeClass('active');
                                        UL.find('li:eq(' + activeIndex + ')').addClass('active');
                                        break;
                                }
                            }
                        });

                        $el.next('.dropdown-menu').on('click', 'li a', function (e) {
                            e.preventDefault();
                            $el.val($(this).text().trim());
                            $('#feedback-city').val($(this).text().trim());
                            console.log('point', $el.val());
                            $el.parent().removeClass('open');
                        });
                    }
                    var UL = $el.next('.dropdown-menu');
                    var p = UL.position();
                    var elpos = $el.position();
                    //alert(p.top+' '+elpos.top);
                    UL.css({'top': elpos.top + 30, 'left': elpos.left, 'width': elpos.width});

                    for (var i in ymapsdata) {
                        //if (ymapsdata[i].GeoObject.metaDataProperty.GeocoderMetaData.kind == 'street') {
                        var item = ymapsdata[i].GeoObject;
                        item.lat = item.Point.pos.split(' ')[0];
                        item.lng = item.Point.pos.split(' ')[1];
                        UL.append('<li><a href="#"  data-id="' + '0' + '"' +
                            (item.lat ? 'data-lat="' + item.lat + '" ' : '') +
                            (item.lng ? 'data-lng="' + item.lng + '" ' : '') +
                            ' data-value="' + item.name + '">'
                            + item.name + '</a></li>');
                        //}
                    }
                    $el.parent().addClass('open');
                } else {
                    $el.parent().removeClass('open');
                }
            }
        });//ajax ends
    });


    // определяем страну пользователя и записываем в куки
    $.ajax({
        url: 'http://freegeoip.net/json/', // country request
        type: 'get',
        dataType: 'json'
    }).done(function (data) {
        console.log('Вы находитесь в стране ' + data.country_name);
        if (data.country_name) {
            window.utils.Cookie.set('country_en', data.country_name); // ENGLISH
            // API yandex translate
            var key = 'trnsl.1.1.20171027T095233Z.b20bc091bce6611d.d93a94431c5550db16997745347c140841b7dbdb';
            var trurl = 'https://translate.yandex.net/api/v1.5/tr.json/translate?'+
            'key='+key+'&text='+data.country_name+'&lang=en-ru'
            $.ajax({
                url: trurl,
                type: 'get',
                dataType: 'json'
            }).done(function (cc) {
                window.utils.Cookie.set('country_ru', cc.text); // RUSSIAN
            });

        }
    });


}); // document.ready

function checkFormErrors() {
    if ($('#Feedback').find('div.has-error').length <= 0 && $('#feedback-fio').val() != '' && $('#feedback-phone').val() != '') {
        $('.btn-submit').attr('disabled', false);
    }
}

function checkFIOField() {
    reFIO = /^[A-Za-zЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЯЧСМИТЬБЮйцукенгшщзхъфывапролджэячсмитьбю ]+$/;
    var test = reFIO.test($('#feedback-fio').val());
    if (!test) {
        $('#feedback-fio').next().html('Укажите ваше настоящее имя').show();
        $('#feedback-fio').parent().addClass('has-error');
        $('.btn-submit').attr('disabled', true);
        var ret = false;
    } else {
        $('#feedback-fio').parent().removeClass('has-error');
        $('#feedback-fio').next().hide();
        var ret = true;
    }

    checkFormErrors();
    return ret;

}


function checkPHONEField() {
    rePHONE = /^([0-9\+\-\(\) )])+$/;
    var test = rePHONE.test($('#feedback-phone').val());
    if (!test) {
        $('#feedback-phone').next().html('Укажите корректный номер телефона').show();
        $('#feedback-phone').parent().addClass('has-error');
        $('.btn-submit').attr('disabled', true);
        var ret = false;
        //console.log(test);
    } else {
        $('#feedback-phone').parent().removeClass('has-error');
        $('#feedback-phone').next().hide();
        var ret = true;
    }
    checkFormErrors();
    return ret;

}

function checkEMAILField() {
    reEmail = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
    var test = reEmail.test($('#feedback-email').val());
    if (!test) {
        $('#feedback-email').next().html('Укажите корректный e-mail').show();
        $('#feedback-email').parent().addClass('has-error');
        $('.btn-submit').attr('disabled', true);
        var ret = false;
        //console.log(test);
    } else {
        $('#feedback-email').parent().removeClass('has-error');
        $('#feedback-email').next().hide();
        var ret = true;
    }
    checkFormErrors();
    return ret;

}

function checkRequiredField(el) {
    var msg = 'Это поле не может быть пустым';
    var s = el.val().replace(/^\s+|\s+$/g, '');
    if (el.attr('id') == 'feedback-text') {
        if (s.length < 10) s = '';
    }
    if (!s) {
        switch (el.attr('id')) {
            case"feedback-fio":
                msg = 'Укажите ваше настоящее имя';
                break;
            case"feedback-phone":
                msg = 'Укажите корректный номер телефона';
                break;
            case"feedback-email":
                msg = 'Укажите корректный e-mail';
                break;
            case"feedback-text":
                msg = 'Введите Ваше сообщение, не менее 10 символов';
                break;
            case"feedback-city":
                msg = 'Введите Ваш город';
                break;
        }
        if(el.attr('id')=='feedback-city') el.next().next().html(msg).show();
        else el.next().html(msg).show();

        el.parent().addClass('has-error');
        $('.btn-submit').attr('disabled', true);
        var ret = false;
    } else {
        el.parent().removeClass('has-error');
        if(el.attr('id')=='feedback-city') el.next().next().hide();
        else el.next().hide();
        var ret = true;
    }

    checkFormErrors();
    return ret;
}

