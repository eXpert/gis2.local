<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\modules\company\models\Rubrics */
?>


<?= $form->field($model, 'parent_id')->dropDownList($rubrics,$params); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

