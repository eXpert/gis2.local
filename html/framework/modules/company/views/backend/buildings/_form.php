<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\modules\company\models\Buildings */
?>

<?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'lat')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'long')->textInput(['maxlength' => true]) ?>

