<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\modules\company\models\Company */
?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'building_id')->dropDownList($buildings,$params); ?>

<!--<?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>-->

