<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\company\models\Company */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('system', 'Company'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">

    <div class="card-header">
        <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
    </div>

    <div class="card-header">
        <p>
            <?= Html::a(Yii::t('system', 'Update'), ['update', 'id' => $model->id], [
                'class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('system', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    </div>

<div class="card-content">

        <?= DetailView::widget([
            'options' => ['class' => 'table'],
            'model' => $model,
            'attributes' => [
                'id',
                'name',

                [
                    'attribute'=>'building_id',
                    'value'=>function($model){
                        return $model->getBuildingName();
                    },

                ],
                [
                    'attribute'=>'phones',
                    'value'=>function($model){
                        foreach($model->phones as $v){
                            $arr[]=$v->phone;
                        }
                        return implode(', ',$arr);
                    },

                ],
                [
                    'attribute'=>'Rubrics',
                    'value'=>function($model){
                        foreach($model->rubrics as $v){
                            $arr[]=$v->name;
                        }
                        return implode(', ',$arr);
                    },

                ],
                //'text:ntext',
            ],
        ]) ?>

    </div>
</div>
