<?php

namespace app\modules\company\models;

/**
 * This is the ActiveQuery class for [[Rubrics]].
 *
 * @see Rubrics
 */
class RubricsQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return Rubrics[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Rubrics|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
