<?php

namespace app\modules\company\models;

/**
 * This is the model class for table "{{%rubrics}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 */
class Rubrics extends \yii\db\ActiveRecord
{


    public function getFirms()
    {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])
            ->viaTable('{{%company_rubrics}}', ['rubric_id' => 'id']);
    }

    /**
     * @return array
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rubrics}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            [['parent_id'], 'integer'],
            ['parent_id', 'default','value'=>'0'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родитель',
            'name' => 'Название',
        ];
    }

    function getRubrics(){
        $arritems = $arr = array();
        $items = $this->find()->all();
        foreach ($items as $item) { //Обходим массив
            $arritems[$item->parent_id][] = $item;
        }

        return $this->outTree($arritems,0,0,$arr);
    }

    function getFlattingRubrics(){
        $arritems = array();
        $items = $this->find()->all();
        foreach ($items as $item) { //Обходим массив
            $arritems[$item->id] = $item->name;
        }

        return $arritems;
    }

    public function getRubricPathString($path,$separator=' / ')
    {
        $strpath = '';
        if(is_array($path)){
            $path = array_reverse($path);
            foreach($path as $item){
                $strpath .= $separator.$item->name;
            }
        }
        return $strpath;
    }

    /**
     * @inheritdoc
     */
    public function getRubricPath($items)
    {
        $path = array();
        $path[] = $this;
        $path = $this->getParentItem($this,$items,$path);
        return $path;
    }

    public function getParentItem($item,$items,&$path){
        if($item->parent_id>0) {
            foreach ($items as $parent) { //Обходим массив
                if ($parent->id == $item->parent_id) {
                    $path[] = $parent;
                    if($parent->parent_id>0){
                        $path = $this->getParentItem($parent,$items,$path);
                    }
                }
            }
        }
        return $path;
    }

    public function getRubricChildRubricList($parent)
    {
        $items = $this->find()->all();
        $path = array();
        $path[] = $parent;
        $path = $this->getChildItem($parent,$items,$path);

        return $path;
    }

    public function getChildItem($item,$items,&$path){

            foreach ($items as $child) { //Обходим массив
                //echo $child->id.': '.$child->parent_id.'<br>';
                if ($item->id == $child->parent_id) {

                    $path[] = $child;
                    $path = $this->getChildItem($child,$items,$path);
                }
            }

        return $path;
    }

    public function outTree($items, $parent_id, $level,&$rubriclist) {
        $arrows = '';
        if (isset($items[$parent_id])) { //Если категория с таким parent_id существует
            foreach ($items[$parent_id] as $value) { //Обходим ее

                for ($i = 0; $i < $level; ++$i) {
                   $arrows .= '>>';
                }
                $rubriclist[$value->id] = $arrows.$value->name;
                $level++; //Увеличиваем уровень вложености
                $this->outTree($items, $value->id, $level,$rubriclist);
                $level--; //Уменьшаем уровень вложености
            }
        }
        return $rubriclist;
    }

    /**
     * @inheritdoc
     * @return RubricsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RubricsQuery(get_called_class());
    }
}
