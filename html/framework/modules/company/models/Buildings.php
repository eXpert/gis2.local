<?php

namespace app\modules\company\models;

/**
 * This is the model class for table "{{%buildings}}".
 *
 * @property integer $id
 * @property string $address
 * @property string $points
 */
class Buildings extends \yii\db\ActiveRecord
{

    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['building_id' => 'id']);
    }

    /**
     * @return array
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%buildings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address'], 'required'],
            [['address'], 'string', 'max' => 255],
            [['lat','long'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Адрес',
            'lat' => 'Широта',
            'long' => 'Долгота',
        ];
    }

    /**
     * @inheritdoc
     * @return BuildingsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BuildingsQuery(get_called_class());
    }
}
