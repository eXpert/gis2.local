<?php

namespace app\modules\company\models;

/**
 * This is the model class for table "{{%phones}}".
 *
 * @property integer $id
 * @property string $phone
 * @property string $label
 */
class Phones extends \yii\db\ActiveRecord
{

    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return array
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%phones}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'label'], 'required'],
            [['phone', 'label'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Phone',
            'label' => 'Label',
        ];
    }

    /**
     * @inheritdoc
     * @return PhonesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PhonesQuery(get_called_class());
    }
}
