<?php

namespace app\modules\company\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%company}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $building_id
 * @property string $text
 */
class Company extends \yii\db\ActiveRecord
{


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhones()
    {
        return $this->hasMany(Phones::className(), ['company_id' => 'id']);
    }

    public function getRubrics()
    {
        return $this->hasMany(Rubrics::className(), ['id' => 'rubric_id'])
            ->viaTable('{{%company_rubrics}}', ['company_id' => 'id']);
    }

    public function getBuilding()
    {
        return $this->hasOne(Buildings::className(), ['id' => 'building_id']);
    }

   

    /**
     * @return array
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'building_id'], 'required'],
            [['building_id'], 'integer'],
            [['text'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'building_id' => 'Здание',
            'text' => 'Текст',
            'phones' => 'Телефон',
            'rubrics' => 'Рубрика',
        ];
    }


    public function getBuildings()
    {
        return ArrayHelper::map(Buildings::find()->all(), 'id', 'address');
    }

    public function getBuildingName()
    {
        return ArrayHelper::getValue(self::getBuildings(), $this->building_id);
    }


    /**
     * @param $r
     * @param $lat
     * @param $long
     * @return Buildings[]|array
     */
    public function getBuildingsFromRadius($r, $lat, $long)
    {

        $buildings = Buildings::find()
            ->select("`id`,`address`, ( 6371 * acos( cos( radians(".$lat." ) ) * cos( radians( `lat` ) ) * cos( radians( `long` ) - radians(".$long.") ) + sin(radians( ".$lat." )) * sin(radians(`lat`)) ) ) as `distance`")
            ->from('{{%buildings}}')
            ->having("`distance` < ".$r)
            ->orderBy('distance')
            ->all();

        return $buildings;
    }

    /**
     * @return Buildings[]|array
     */
    public function getBuildingsFromSquare()
    {
        $lat1 = (float)Yii::$app->request->get('lat1');   //83.63;
        $long1 = (float)Yii::$app->request->get('long1'); //53.32;
        $lat2 = (float)Yii::$app->request->get('lat2');   //86.19;
        $long2 = (float)Yii::$app->request->get('long2'); //55.33;
        $geometry = "POLYGON(($lat1 $long1, $lat2 $long1, $lat2 $long2, $lat1 $long2, $lat1 $long1))";
        $buildings = Buildings::find()
            ->select("`id`,`address`,`lat`,`long`")
            ->from('{{%buildings}}')
            ->where("st_contains(st_geomfromtext( '$geometry' ), `geom`) = 1")
            ->all();
        return $buildings;
    }

    /**
     * @inheritdoc
     * @return CompanyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }
}
