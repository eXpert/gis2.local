<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company`.
 */
class m171104_090547_create_company_table extends Migration
{

    private $_tableName = '{{%company}}';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'building_id' => $this->integer(),
            'text' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc ...
     */
    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
