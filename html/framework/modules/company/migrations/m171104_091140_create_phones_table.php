<?php

use yii\db\Migration;

/**
 * Handles the creation of table `phones`.
 */
class m171104_091140_create_phones_table extends Migration
{
    private $_tableName = '{{%phones}}';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => $this->primaryKey(),
            'phone' => $this->string(50),
            'label' => $this->string(255),
            'company_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
