<?php

use yii\db\Migration;

/**
 * Handles the creation of table `buildings`.
 */
class m171104_091414_create_buildings_table extends Migration
{
    private $_tableName = '{{%buildings}}';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => $this->primaryKey(),
            'address' => $this->string(255),
            'lat' => $this->string(50),
            'long' => $this->string(50),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
