<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rubrics`.
 */
class m171104_091607_create_rubrics_table extends Migration
{
    private $_tableName = '{{%rubrics}}';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->_tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'parent_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
