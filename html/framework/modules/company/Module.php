<?php

namespace app\modules\company;

use krok\system\components\backend\NameInterface;
use Yii;

/**
* company module definition class
*/
class Module extends \yii\base\Module implements NameInterface
{
/**
* @inheritdoc
*/
public $controllerNamespace = null;
    /**
     * @var string
     */
    public $defaultRoute = 'company';

/**
* @inheritdoc
*/
public function init()
{
parent::init();
}

/**
* @return string
*/
public static function getName()
{
return Yii::t('system', 'Company');
}
}
