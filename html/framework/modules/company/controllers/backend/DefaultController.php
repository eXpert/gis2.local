<?php

namespace app\modules\company\controllers\backend;

use krok\system\components\backend\Controller;

/**
 * Default controller for the `company` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
