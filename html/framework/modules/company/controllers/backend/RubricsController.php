<?php

namespace app\modules\company\controllers\backend;

use Yii;
use app\modules\company\models\Rubrics;
use app\modules\company\models\RubricsSearch;
use krok\system\components\backend\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * RubricsController implements the CRUD actions for Rubrics model.
 */
class RubricsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rubrics models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RubricsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $items = new Rubrics();
        $items = $items->find()->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'items' => $items,
        ]);
    }

    /**
     * Displays a single Rubrics model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $rubrics = new Rubrics();
        $rubrics = $rubrics->getFlattingRubrics();
        $items = new Rubrics();
        $items = $items->find()->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'rubrics'=>$rubrics,
            'items' => $items,
        ]);
    }

    /**
     * Creates a new Rubrics model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {



        $items = new Rubrics();
        $items = $items->getRubrics();
        $params = [
            'prompt' => 'Выберите рубрику'
        ];

        $model = new Rubrics();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'rubrics' => $items,
                'params' => $params
            ]);
        }
    }

    /**
     * Updates an existing Rubrics model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $items = new Rubrics();
        $items = $items->getRubrics();
        unset($items[$id]);
        $params = [
            'prompt' => 'Выберите рубрику'
        ];


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'rubrics' => $items,
                'params' => $params
            ]);
        }
    }

    /**
     * Deletes an existing Rubrics model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Rubrics model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rubrics the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rubrics::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
