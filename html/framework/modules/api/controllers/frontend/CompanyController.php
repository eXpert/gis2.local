<?php

namespace app\modules\api\controllers\frontend;

use app\modules\company\models\Buildings;
use Yii;
use app\modules\company\models\Company;
use yii\web\NotFoundHttpException;

class CompanyController extends DefaultController
{
    public function actions()
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::class,
            ],
        ];
    }
    public function actionList()
    {
        $data = Company::find()->select(['id','name'])->asArray()->all();
        return parent::sendResponse($data);
    }

    public function actionView($id)
    {
        $model = Company::find()->where(['id'=>$id])->one();

        $data = [
            'company' => $model,
            'phones' => $model->phones,
            'rubrics' => $model->rubrics,
            'building' => $model->building
        ];
        return parent::sendResponse($data);
    }

    public function actionLike($name)
    {
        $data = Company::find()->where(['like','name',$name])->select(['id','name'])->asArray()->all();
        return parent::sendResponse($data);
    }

    public function actionRadius()
    {
        if(isset($_GET['r']) &&  isset($_GET['lat']) && isset($_GET['long']) ){

            $companies = [];
            $company = new Company;
            $buildings = $company->getBuildingsFromRadius(Yii::$app->request->get('r'),Yii::$app->request->get('lat'),Yii::$app->request->get('long'));
            if(count($buildings)>0){
                foreach($buildings as $building){
                    $companies[] = Company::find()->where(['building_id'=>$building->id])->all();
                }
            }
            $data = [
                'buildings' => $buildings,
                'company' => $companies
            ];
            return parent::sendResponse($data);
        } else {
            throw new NotFoundHttpException('Вы использовали не правильный запрос к API');
        }

    }

    public function actionSquare()
    {



        if(isset($_GET['lat1']) &&  isset($_GET['lat2']) && isset($_GET['long1']) && isset($_GET['long2']) ){

            $companies=[];
            $company = new Company;
            $buildings = $company->getBuildingsFromSquare();
            if(count($buildings)>0){
                foreach($buildings as $building){
                    $companies[] = Company::find()->where(['building_id'=>$building->id])->all();
                }
            }
            $data = [
                'buildings' => $buildings,
                'company' => $companies
            ];
            return parent::sendResponse($data);
        } else {
            throw new NotFoundHttpException('Вы использовали не правильный запрос к API');
        }

    }

    // переиндексирует/создает геометрии для зданий
    public function actionGeom()
    {
        $builds = Buildings::find()->all();
        foreach($builds as $b){
            Yii::$app->db->createCommand(" UPDATE {{%buildings}} SET `geom` = GeomFromText('POINT(".$b->lat." ".$b->long.")') WHERE `id` = :id ")
                ->bindValue(':id',$b->id)
                ->execute();
        }

    }


}
