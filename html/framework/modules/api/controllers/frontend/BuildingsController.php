<?php

namespace app\modules\api\controllers\frontend;


use app\modules\company\models\Company;
use app\modules\company\models\Buildings;


class BuildingsController extends DefaultController
{
    public function actions()
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::class,
            ],
        ];
    }

    public function actionList()
    {
        $data = Buildings::find()->select(['id','address'])->asArray()->all();
        return parent::sendResponse($data);
    }

    public function actionView($id)
    {
        $model = Buildings::find()->where(['id'=>$id])->select(['id','address'])->one();

        $data = [
            'building' => $model,
            'company' => $model->companies
        ];
        return parent::sendResponse($data);
    }

    public function actionLike($address)
    {
        $data = Buildings::find()->where(['like','address',$address])->select(['id','address'])->asArray()->all();
        return parent::sendResponse($data);
    }



}
