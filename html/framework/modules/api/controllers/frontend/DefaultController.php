<?php

namespace app\modules\api\controllers\frontend;

use Yii;
use krok\system\components\frontend\Controller;
use yii\web\Response;

/**
 * Default controller for the `api` module
 */
class DefaultController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::class,
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function sendResponse($data)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [
            'response'=> $data
        ];
        return $response;
    }

    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception != null) {
            if ($exception instanceof HttpException) {
                $data = [
                    'error'=> ['exception' => $exception]
                ];
            }
        }
        return $this->sendResponse($data);
    }




}
