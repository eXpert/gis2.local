<?php

namespace app\modules\api\controllers\frontend;

use Yii;
use app\modules\company\models\Rubrics;

class RubricsController extends DefaultController
{
    public function actions()
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::class,
            ],
        ];
    }
    public function actionList()
    {
        $data = Rubrics::find()->select(['id','name'])->asArray()->all();
        return parent::sendResponse($data);
    }

    public function actionView($id)
    {
        $data = Rubrics::find()->where(['id'=>$id])->select(['id','name'])->asArray()->all();
        return parent::sendResponse($data);
    }

    public function actionCompany($id)
    {
        $model = Rubrics::find()->where(['id'=>$id])->select(['id','name'])->one();

        if(Yii::$app->request->get('all')){
            // выдаем все компании включая подрубрики
            $rubrics = new Rubrics;
            $ChildRubric = $rubrics->getRubricChildRubricList($model);
            if(count($ChildRubric)>0 && is_array($ChildRubric)){
                foreach($ChildRubric as $rubric){
                    if($rubric->firms) $firms[] = $rubric->firms;
                }
            }

        } else {
            $firms = $model->firms;
        }

        $data = [
            'rubrica' => $model,
            'company' => $firms
        ];
        return parent::sendResponse($data);
    }

    public function actionCompanysearch($name)
    {
        $model = Rubrics::find()->where(['like','name',$name])->select(['id','name'])->one();

        if(Yii::$app->request->get('all')){
            // выдаем все компании включая подрубрики
            $rubrics = new Rubrics;
            $ChildRubric = $rubrics->getRubricChildRubricList($model);
            if(count($ChildRubric)>0 && is_array($ChildRubric)){
                foreach($ChildRubric as $rubric){
                    if($rubric->firms) $firms[] = $rubric->firms;
                }
            }

        } else {
            $firms = $model->firms;
        }

        $data = [
            'rubrica' => $model,
            'company' => $firms
        ];
        return parent::sendResponse($data);
    }

    public function actionLike($name)
    {
        $data = Rubrics::find()->where(['like','name',$name])->select(['id','name'])->asArray()->all();
        return parent::sendResponse($data);
    }

   
}
