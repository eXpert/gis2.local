<?php

namespace app\modules\api\controllers\frontend;

use Yii;
use app\modules\company\models\Company;
use app\modules\company\models\Phones;

class PhonesController extends DefaultController
{
    public function actions()
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::class,
            ],
        ];
    }
    public function actionList()
    {
        $where = [];
        if(Yii::$app->request->get('company_id')){
            if(Yii::$app->request->get('company_id')>0){
                $where = ['company_id'=>Yii::$app->request->get('company_id')];
            }
        }
        $data = Phones::find()->select(['id','phone'])->where($where)->asArray()->all();
        return parent::sendResponse($data);
    }

    public function actionView($id)
    {
        $data = Phones::find()->where(['id'=>$id])->select(['id','phone'])->asArray()->all();
        return parent::sendResponse($data);
    }

    public function actionLike($phone)
    {
        $data = Phones::find()->where(['like','phone',$phone])->select(['id','phone'])->asArray()->all();
        return parent::sendResponse($data);
    }

}
